package dev.mateusbandeira.scbinventario.controller;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.http.HttpStatus;

import dev.mateusbandeira.scbinventario.model.Totem;
import dev.mateusbandeira.scbinventario.model.Totem.TotemJsonSchema;
import dev.mateusbandeira.scbinventario.repository.TotemRepository;
import io.javalin.http.Context;

public class TotemController {
	private TotemController() {
		super();
	}

	public static void createTotem(Context ctx) {
		Totem totem = getResourceFromContext(ctx);
		
		if(totem == null) {
			ctx.status(HttpStatus.BAD_REQUEST_400);
			return;
		}
		
		totem = totem.withoutUuid();
		
		new TotemRepository().insert(totem);
		ctx.json(new Totem.TotemJsonSchema(totem));
		ctx.status(HttpStatus.OK_200);
	}

	public static void getAllTotems(Context ctx) {
		List<Totem> findAll = new TotemRepository().findAll();
		List<TotemJsonSchema> results = new ArrayList<>();
		for(Totem totem : findAll) {
			results.add(new TotemJsonSchema(totem));
		}
		ctx.json(results);
		ctx.status(HttpStatus.OK_200);
	}

	public static void getTotem(Context ctx) {
		Totem totem = getResourceFromContext(ctx);

		if (totem == null || totem.getUuid() == null) {
			getAllTotems(ctx);
			return;
		}

		Totem retrievedTotem = new TotemRepository().findById(totem.getUuid());
		if (retrievedTotem != null) {
			ctx.json(new TotemJsonSchema(retrievedTotem));
			ctx.status(HttpStatus.OK_200);
		} else {
			ctx.status(HttpStatus.NOT_FOUND_404);
		}
	}

	public static void deleteTotem(Context ctx) {
		Totem totem = getResourceFromContext(ctx);
		if(totem == null) {
			ctx.status(HttpStatus.NOT_FOUND_404);
			return;
		}
		
		Totem deleteById = new TotemRepository().deleteById(totem.getUuid());
		
		if(deleteById == null) {
			ctx.status(HttpStatus.NOT_FOUND_404);
			return;
		}
		ctx.json(new TotemJsonSchema(deleteById));
		ctx.status(HttpStatus.OK_200);
	}

	protected static Totem getResourceFromContext(Context ctx) {
		try {
			return ctx.bodyAsClass(Totem.TotemJsonSchema.class).getAsModel();
		} catch (Exception ex) {
			return null;
		}
	}

}
