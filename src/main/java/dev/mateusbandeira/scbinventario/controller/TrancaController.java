package dev.mateusbandeira.scbinventario.controller;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.http.HttpStatus;

import dev.mateusbandeira.scbinventario.model.StatusTranca;
import dev.mateusbandeira.scbinventario.model.Tranca;
import dev.mateusbandeira.scbinventario.repository.TrancaRepository;
import io.javalin.http.Context;

public class TrancaController {

	private TrancaController() {
		super();
	}

	public static void createTranca(Context ctx) {
		Tranca tranca = getResourceFromCtx(ctx);

		if (tranca == null) {
			ctx.status(HttpStatus.BAD_REQUEST_400);
			return;
		}

		tranca = tranca.withoutUuid();

		new TrancaRepository().insert(tranca);
		ctx.json(new Tranca.TrancaJsonSchema(tranca));
		ctx.status(HttpStatus.OK_200);
	}

	public static void getAllTrancas(Context ctx) {
		List<Tranca.TrancaJsonSchema> results = new ArrayList<>();
		for(Tranca tranca : new TrancaRepository().findAll()) {
			results.add(new Tranca.TrancaJsonSchema(tranca));
		}
		ctx.json(results);
		ctx.status(HttpStatus.OK_200);
	}

	public static void getTranca(Context ctx) {
		Tranca tranca = getResourceFromCtx(ctx);

		if (tranca == null || tranca.getUuid() == null) {
			getAllTrancas(ctx);
			return;
		}

		Tranca retrievedTranca = new TrancaRepository().findById(tranca.getUuid());
		if (retrievedTranca != null) {
			ctx.json(new Tranca.TrancaJsonSchema(retrievedTranca));
			ctx.status(HttpStatus.OK_200);
		} else {
			ctx.status(HttpStatus.NOT_FOUND_404);
		}
	}
	
	public static void getStatusTranca(Context ctx) {
		Tranca tranca = getResourceFromCtx(ctx);

		if (tranca == null || tranca.getUuid() == null) {
			ctx.status(HttpStatus.NOT_FOUND_404);
			return;
		}

		Tranca retrievedTranca = new TrancaRepository().findById(tranca.getUuid());
		if (retrievedTranca != null) {
			ctx.json(new Tranca.TrancaJsonSchema(retrievedTranca).isOcupada());
			ctx.status(HttpStatus.OK_200);
		} else {
			ctx.status(HttpStatus.NOT_FOUND_404);
		} 
	}
	
	public static void setStatusTranca(Context ctx) {
		Tranca tranca = getResourceFromCtx(ctx);

		if (tranca == null || tranca.getUuid() == null) {
			ctx.status(HttpStatus.NOT_FOUND_404);
			return;
		}

		Tranca retrievedTranca = new TrancaRepository().findById(tranca.getUuid());
		retrievedTranca.setStatus(tranca.getStatus());
		new TrancaRepository().insertOrUpdate(retrievedTranca.getUuid(), retrievedTranca);
		ctx.status(200);
	}
	
	public static void liberaTranca(Context ctx) {
		Tranca tranca = getResourceFromCtx(ctx);

		if (tranca == null || tranca.getUuid() == null) {
			ctx.status(HttpStatus.NOT_FOUND_404);
			return;
		}

		Tranca retrievedTranca = new TrancaRepository().findById(tranca.getUuid());
		if(!retrievedTranca.getStatus().equals(StatusTranca.TRANCADA)) {
			if(retrievedTranca.getStatus().equals(StatusTranca.DESTRANCADA)) {
				ctx.status(HttpStatus.NOT_ACCEPTABLE_406).result("Tranca já se encontra destrancada.");
			} else {
				ctx.status(HttpStatus.NOT_ACCEPTABLE_406).result("Tranca não pode ser destrancada.");
			}
		}
		retrievedTranca.setStatus(StatusTranca.DESTRANCADA);
		new TrancaRepository().insertOrUpdate(retrievedTranca.getUuid(), retrievedTranca);
		ctx.status(HttpStatus.OK_200);
	}

	public static void deleteTranca(Context ctx) {
		Tranca tranca = getResourceFromCtx(ctx);
		if (tranca == null) {
			ctx.status(HttpStatus.BAD_REQUEST_400);
			return;
		}
		Tranca deleteById = new TrancaRepository().deleteById(tranca.getUuid());
		if(deleteById == null) {
			ctx.status(HttpStatus.NOT_FOUND_404);
			return;
		}
		ctx.json(new Tranca.TrancaJsonSchema(deleteById));
		ctx.status(HttpStatus.OK_200);
	}

	private static Tranca getResourceFromCtx(Context ctx) {
		try {
			return ctx.bodyAsClass(Tranca.TrancaJsonSchema.class).getAsModel();
		} catch (Exception ex) {
			return null;
		}
	}
}
