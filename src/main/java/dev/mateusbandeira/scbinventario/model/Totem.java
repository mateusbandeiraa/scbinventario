package dev.mateusbandeira.scbinventario.model;

import java.beans.Transient;
import java.util.Objects;
import java.util.UUID;

public class Totem implements Identifiable<Totem>{
	private UUID uuid;
	private String numero;

	public Totem(UUID uuid, String numero) {
		super();
		this.uuid = uuid;
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Totem [uuid=" + uuid + ", numero=" + numero + "]";
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public Totem withoutUuid() {
		return new Totem(null, numero);
	}

	@Override
	public int hashCode() {
		return Objects.hash(numero, uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Totem))
			return false;
		Totem other = (Totem) obj;
		return Objects.equals(numero, other.numero) && Objects.equals(uuid, other.uuid);
	}

	public static class TotemJsonSchema {
		String uuid;
		String numero;

		public TotemJsonSchema(Totem t) {
			if (t.getUuid() != null) {
				this.uuid = t.getUuid().toString();
			}
			this.numero = t.numero;
		}

		public TotemJsonSchema() {
			super();
		}

		@Override
		public String toString() {
			return "TotemJsonSchema [uuid=" + uuid + ", numero=" + numero + "]";
		}

		@Override
		public int hashCode() {
			return Objects.hash(numero, uuid);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof TotemJsonSchema))
				return false;
			TotemJsonSchema other = (TotemJsonSchema) obj;
			return Objects.equals(numero, other.numero) && Objects.equals(uuid, other.uuid);
		}

		@Transient
		public Totem getAsModel() {
			UUID parsedUuid;
			try {
				parsedUuid = UUID.fromString(this.uuid);
			} catch (NullPointerException | IllegalArgumentException ex) {
				/*
				 * NullPointerException for when this.uuid is null
				 * IllegalArgumentException for when this.uuid is invalid
				 */
				parsedUuid = null;
			}
			return new Totem(parsedUuid, numero);
		}

		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

	}
}
