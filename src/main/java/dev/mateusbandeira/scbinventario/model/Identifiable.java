package dev.mateusbandeira.scbinventario.model;

import java.util.UUID;

public interface Identifiable<T> {
	
	public UUID getUuid();

	public void setUuid(UUID uuid);

	public T withoutUuid();

}
