package dev.mateusbandeira.scbinventario.model;

public enum StatusTranca {
	TRANCADA, DESTRANCADA, APOSENTADA, REPARO;
}
