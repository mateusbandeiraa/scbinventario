package dev.mateusbandeira.scbinventario.model;

import static dev.mateusbandeira.scbinventario.model.StatusTranca.*;

import java.beans.Transient;
import java.util.Objects;
import java.util.UUID;

public class Tranca implements Identifiable<Tranca> {
	private UUID uuid;
	private String numero;
	private StatusTranca status;

	public Tranca(UUID uuid, String numero, StatusTranca status) {
		super();
		this.uuid = uuid;
		this.numero = numero;
		this.status = status;
	}

	public Tranca(String numero, StatusTranca status) {
		this(null, numero, status);
	}

	Tranca() {
		super();
	}

	@Override
	public String toString() {
		return "Tranca [uuid=" + uuid + ", numero=" + numero + ", status=" + status + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(numero, status, uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Tranca))
			return false;
		Tranca other = (Tranca) obj;
		return Objects.equals(numero, other.numero) && status == other.status
				&& Objects.equals(uuid, other.uuid);
	}

	@Override
	public Tranca withoutUuid() {
		return new Tranca(numero, status);
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public StatusTranca getStatus() {
		return status;
	}

	public void setStatus(StatusTranca status) {
		this.status = status;
	}

	public static class TrancaJsonSchema {
		String uuid;
		String numero;
		boolean ocupada;

		public TrancaJsonSchema(Tranca t) {
			this.numero = t.numero;
			this.ocupada = t.status == TRANCADA;
			if (t.uuid != null) {
				this.uuid = t.uuid.toString();
			}
		}

		public TrancaJsonSchema() {
			super();
		}

		@Override
		public int hashCode() {
			return Objects.hash(numero, ocupada, uuid);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof TrancaJsonSchema))
				return false;
			TrancaJsonSchema other = (TrancaJsonSchema) obj;
			return Objects.equals(numero, other.numero) && ocupada == other.ocupada
					&& Objects.equals(uuid, other.uuid);
		}

		@Transient
		public Tranca getAsModel() {
			UUID parsedUuid;
			try {
				parsedUuid = UUID.fromString(this.uuid);
			} catch (NullPointerException | IllegalArgumentException ex) {
				/*
				 * NullPointerException for when this.uuid is null
				 * IllegalArgumentException for when this.uuid is invalid
				 */
				parsedUuid = null;
			}
			return new Tranca(parsedUuid, numero, ocupada ? TRANCADA : DESTRANCADA);
		}

		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		public boolean isOcupada() {
			return ocupada;
		}

		public void setOcupada(boolean ocupada) {
			this.ocupada = ocupada;
		}

	}

}
