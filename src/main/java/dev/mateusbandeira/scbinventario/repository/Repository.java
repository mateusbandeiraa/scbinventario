package dev.mateusbandeira.scbinventario.repository;

import java.util.List;

public interface Repository<T, PK> {

	public void insert(T t);

	public void insertOrUpdate(PK id, T t);

	public T findById(PK id);

	public List<T> findAll();

	public T deleteById(PK id);
	
	public void deleteAll();

	class DuplicateEntryException extends RuntimeException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 154858849314844737L;

	}
}
