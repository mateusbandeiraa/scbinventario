package dev.mateusbandeira.scbinventario.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import dev.mateusbandeira.scbinventario.model.Totem;

public class TotemRepository extends IdentifiableRepository<Totem> {

	private static final Map<UUID, Totem> TOTEMS = new HashMap<>();

	@Override
	protected Map<UUID, Totem> getItemsMap() {
		return TOTEMS;
	}

	@Override
	public void deleteAll() {
		TOTEMS.clear();
	}

}
