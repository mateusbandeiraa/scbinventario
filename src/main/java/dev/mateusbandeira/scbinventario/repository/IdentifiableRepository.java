package dev.mateusbandeira.scbinventario.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import dev.mateusbandeira.scbinventario.model.Identifiable;

public abstract class IdentifiableRepository<T extends Identifiable<T>> implements Repository<T, UUID>{
	
	protected abstract Map<UUID, T> getItemsMap();

	@Override
	public void insert(T t) {
		if (t.getUuid() == null) {
			t.setUuid(UUID.randomUUID());
		}
		if (getItemsMap().containsKey(t.getUuid())) {
			throw new DuplicateEntryException();
		}
		this.insertOrUpdate(t.getUuid(), t);
	}

	@Override
	public void insertOrUpdate(UUID id, T t) {
		if (id == null) {
			throw new IllegalArgumentException();
		}
		t.setUuid(id);
		getItemsMap().put(t.getUuid(), t);
	}

	@Override
	public T findById(UUID id) {
		return getItemsMap().get(id);
	}

	@Override
	public List<T> findAll() {
		return new ArrayList<>(getItemsMap().values());
	}

	@Override
	public T deleteById(UUID id) {
		return getItemsMap().remove(id);
	}

}
