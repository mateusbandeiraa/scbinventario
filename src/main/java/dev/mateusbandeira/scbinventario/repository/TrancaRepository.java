package dev.mateusbandeira.scbinventario.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import dev.mateusbandeira.scbinventario.model.Tranca;

public class TrancaRepository extends IdentifiableRepository<Tranca> {

	private static final Map<UUID, Tranca> TRANCAS = new HashMap<>();

	@Override
	protected Map<UUID, Tranca> getItemsMap() {
		return TRANCAS;
	}

	@Override
	public void deleteAll() {
		TRANCAS.clear();
	}
}
