package dev.mateusbandeira.scbinventario;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dev.mateusbandeira.scbinventario.controller.TotemController;
import dev.mateusbandeira.scbinventario.controller.TrancaController;
import io.javalin.Javalin;

public class Application {
	private static final int DEFAULT_PORT = 7000;
	private static Logger logger = LoggerFactory.getLogger(Application.class);
	private Javalin app = Javalin.create(config -> {
		config.defaultContentType = "application/json";
		config.enableCorsForAllOrigins();
	}).routes(() -> {
		path("tranca", () -> {
			get(TrancaController::getTranca);
			post(TrancaController::createTranca);
			delete(TrancaController::deleteTranca);
		});
		path("statusTranca", () -> {
			get(TrancaController::getStatusTranca);
			post(TrancaController::setStatusTranca);
		});
		path("liberaTranca", () -> {
			post(TrancaController::liberaTranca);
		});
		path("totem", () -> {
			get(TotemController::getAllTotems);
			post(TotemController::createTotem);
			delete(TotemController::deleteTotem);
		});
	});

	public static void main(String[] args) {
		int port = DEFAULT_PORT;
		if (System.getenv("PORT") != null) {
			port = Integer.valueOf(System.getenv("PORT"));
		}
		new Application().start(port);
	}

	public void start(int port) {
		logger.info("Will try to startup on port " + port);
		this.app.start(port);
		logger.info("Application started on port " + port);
	}

	public void stop() {
		this.app.stop();
		logger.info("Stopped server.");
	}
}
