package dev.mateusbandeira.scbinventario.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import dev.mateusbandeira.scbinventario.model.Totem;
import dev.mateusbandeira.scbinventario.model.Totem.TotemJsonSchema;
import dev.mateusbandeira.scbinventario.repository.TotemRepository;
import io.javalin.http.Context;

class TotemControllerTest {

	Context ctx = mock(Context.class);
	final String UUID_1 = "63879236-c5c5-4c9a-9749-0c46130b947d";

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
		new TotemRepository().deleteAll();
	}

	@Test
	void testCreateTotemReturnsJson() {
		TotemJsonSchema totemJsonSchema = new Totem.TotemJsonSchema();
		totemJsonSchema.setNumero("0001");
		totemJsonSchema.setUuid(UUID_1);
		when(ctx.bodyAsClass(Totem.TotemJsonSchema.class)).thenReturn(totemJsonSchema);
		TotemController.createTotem(ctx);
		verify(ctx).json(any(Totem.TotemJsonSchema.class));
		verify(ctx).status(HttpStatus.OK_200);
	}

	@Test
	void testCreateNullTotemReturns400() {
		when(ctx.bodyAsClass(Totem.TotemJsonSchema.class)).thenReturn(null);
		TotemController.createTotem(ctx);
		verify(ctx).status(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	void testGetAllTotems() {
		List<TotemJsonSchema> sentTotemJsonSchemas = new ArrayList<>();

		/* Creating totems with numeros 0001, 0002 & 0003 */
		for (int i = 1; i <= 3; i++) {
			Totem totem = new Totem(null, "000" + i);

			new TotemRepository().insert(totem);

			sentTotemJsonSchemas.add(new TotemJsonSchema(totem));
		}

		TotemController.getTotem(ctx);

		@SuppressWarnings("rawtypes")
		ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
		verify(ctx).json(argument.capture());

		@SuppressWarnings("unchecked")
		List<TotemJsonSchema> capturedTotemJsonSchemas = argument.getValue();

		/*
		 * We need to sort both lists beforehands because List#equals
		 * only return true if both lists have the same elements AND they are in the
		 * same order
		 */
		Comparator<TotemJsonSchema> totemJsonSchemaComparator = new Comparator<TotemJsonSchema>() {

			@Override
			public int compare(TotemJsonSchema o1, TotemJsonSchema o2) {
				return o1.getUuid().compareTo(o2.getUuid());
			}
		};

		Collections.sort(sentTotemJsonSchemas, totemJsonSchemaComparator);
		Collections.sort(capturedTotemJsonSchemas, totemJsonSchemaComparator);

		assertEquals(sentTotemJsonSchemas, capturedTotemJsonSchemas);
		verify(ctx).status(HttpStatus.OK_200);
	}

	@Test
	void testGetValidTotemReturns200() {
		Totem totem = new Totem(UUID.fromString(UUID_1), "0001");
		new TotemRepository().insert(totem);
		
		TotemJsonSchema totemJsonSchema = new TotemJsonSchema(totem);
		
		when(ctx.bodyAsClass(Totem.TotemJsonSchema.class)).thenReturn(totemJsonSchema);
		TotemController.getTotem(ctx);
		
		verify(ctx).json(totemJsonSchema);
		verify(ctx).status(HttpStatus.OK_200);
	}
	
	@Test
	void testGetInvalidTotemReturns404() {
		Totem totem = new Totem(UUID.fromString(UUID_1), "0001");
		
		TotemJsonSchema totemJsonSchema = new TotemJsonSchema(totem);
		
		when(ctx.bodyAsClass(Totem.TotemJsonSchema.class)).thenReturn(totemJsonSchema);
		TotemController.getTotem(ctx);
		
		verify(ctx, times(0)).json(any());
		verify(ctx).status(HttpStatus.NOT_FOUND_404);
	}

	@Test
	void testDeleteExistingTotemReturnsJson() {
		Totem totem = new Totem(UUID.fromString(UUID_1), "0001");
		new TotemRepository().insert(totem);
		
		TotemJsonSchema totemJsonSchema = new TotemJsonSchema(totem);
		when(ctx.bodyAsClass(Totem.TotemJsonSchema.class)).thenReturn(totemJsonSchema);
		
		TotemController.deleteTotem(ctx);
		
		ArgumentCaptor<TotemJsonSchema> argumentCaptor = ArgumentCaptor.forClass(TotemJsonSchema.class);
		verify(ctx).json(argumentCaptor.capture());
		verify(ctx).status(HttpStatus.OK_200);
		
		TotemJsonSchema capturedTotemJsonSchema = argumentCaptor.getValue();
		TotemJsonSchema expectedTotemJsonSchema = new TotemJsonSchema(totem);
		assertEquals(expectedTotemJsonSchema, capturedTotemJsonSchema);
	}
	
	@Test
	void testDeleteInvalidTotemReturns404() {
		when(ctx.bodyAsClass(Totem.TotemJsonSchema.class)).thenReturn(null);
		
		TotemController.deleteTotem(ctx);
		
		verify(ctx).status(HttpStatus.NOT_FOUND_404);
	}
	
	@Test
	void testDeleteNonExistentTotemReturns404() {
		Totem totem = new Totem(UUID.fromString(UUID_1), "0001");
		
		TotemJsonSchema totemJsonSchema = new TotemJsonSchema(totem);
		when(ctx.bodyAsClass(Totem.TotemJsonSchema.class)).thenReturn(totemJsonSchema);
		
		TotemController.deleteTotem(ctx);
		
		verify(ctx).status(HttpStatus.NOT_FOUND_404);
	}

}