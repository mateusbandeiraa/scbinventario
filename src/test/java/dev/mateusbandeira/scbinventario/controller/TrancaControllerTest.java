package dev.mateusbandeira.scbinventario.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import dev.mateusbandeira.scbinventario.model.StatusTranca;
import dev.mateusbandeira.scbinventario.model.Tranca;
import dev.mateusbandeira.scbinventario.model.Tranca.TrancaJsonSchema;
import dev.mateusbandeira.scbinventario.repository.TrancaRepository;
import io.javalin.http.Context;

class TrancaControllerTest {

	Context ctx = mock(Context.class);
	final String UUID_1 = "63879236-c5c5-4c9a-9749-0c46130b947d";

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
		new TrancaRepository().deleteAll();
	}

	@Test
	void testCreateTrancaReturnsJson() {
		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema();
		trancaJsonSchema.setNumero("0001");
		trancaJsonSchema.setOcupada(false);
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(trancaJsonSchema);
		TrancaController.createTranca(ctx);

		ArgumentCaptor<TrancaJsonSchema> argumentCaptor = ArgumentCaptor
				.forClass(trancaJsonSchema.getClass());

		verify(ctx).json(argumentCaptor.capture());

		TrancaJsonSchema capturedTrancaJsonSchema = argumentCaptor.getValue();

		assertNotNull(capturedTrancaJsonSchema.getUuid());
		assertEquals(trancaJsonSchema.getNumero(), capturedTrancaJsonSchema.getNumero());
		assertEquals(trancaJsonSchema.isOcupada(), capturedTrancaJsonSchema.isOcupada());
		verify(ctx).status(HttpStatus.OK_200);
	}

	@Test
	void testCreateTrancaInsertsToRepository() {
		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema();
		trancaJsonSchema.setNumero("0001");
		trancaJsonSchema.setOcupada(false);
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(trancaJsonSchema);

		TrancaController.createTranca(ctx);

		ArgumentCaptor<TrancaJsonSchema> argumentCaptor = ArgumentCaptor
				.forClass(trancaJsonSchema.getClass());

		verify(ctx).json(argumentCaptor.capture());
		verify(ctx).status(HttpStatus.OK_200);

		TrancaJsonSchema capturedTrancaJsonSchema = argumentCaptor.getValue();
		assertNotNull(capturedTrancaJsonSchema.getUuid());

		Tranca findById = new TrancaRepository()
				.findById(UUID.fromString(capturedTrancaJsonSchema.getUuid()));

		assertEquals(trancaJsonSchema.getNumero(), findById.getNumero());
	}

	@Test
	void testCreateTrancaReturnsDifferentUuid() {
		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema();
		trancaJsonSchema.setNumero("0001");
		trancaJsonSchema.setOcupada(false);
		trancaJsonSchema.setUuid(UUID_1);
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(trancaJsonSchema);
		TrancaController.createTranca(ctx);

		ArgumentCaptor<TrancaJsonSchema> argumentCaptor = ArgumentCaptor
				.forClass(trancaJsonSchema.getClass());

		verify(ctx).json(argumentCaptor.capture());

		TrancaJsonSchema capturedTrancaJsonSchema = argumentCaptor.getValue();

		assertNotEquals(trancaJsonSchema.getUuid(), capturedTrancaJsonSchema.getUuid());
		verify(ctx).status(HttpStatus.OK_200);
	}

	@Test
	void testCreateInvalidTrancaReturns400() {
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(null);
		TrancaController.createTranca(ctx);

		verify(ctx).status(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	void testGetTrancaReturnsJson() {
		Tranca tranca = new Tranca("0001", StatusTranca.DESTRANCADA);
		new TrancaRepository().insert(tranca);

		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema();
		trancaJsonSchema.setUuid(tranca.getUuid().toString());
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(trancaJsonSchema);

		TrancaController.getTranca(ctx);

		ArgumentCaptor<TrancaJsonSchema> argumentCaptor = ArgumentCaptor
				.forClass(trancaJsonSchema.getClass());

		verify(ctx).json(argumentCaptor.capture());
		verify(ctx).status(HttpStatus.OK_200);

		TrancaJsonSchema capturedTrancaJsonSchema = argumentCaptor.getValue();
		TrancaJsonSchema expectedTrancaJsonSchema = new TrancaJsonSchema(tranca);

		assertEquals(expectedTrancaJsonSchema, capturedTrancaJsonSchema);
	}

	@Test
	void testGetInvalidTrancaReturns404() {
		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema();
		trancaJsonSchema.setUuid(UUID_1);
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(trancaJsonSchema);

		TrancaController.getTranca(ctx);

		verify(ctx).status(HttpStatus.NOT_FOUND_404);
	}

	@Test
	void testGetAllTrancasReturnsJson() {
		List<TrancaJsonSchema> sentTrancaJsonSchemas = new ArrayList<>();
		for (int i = 1; i <= 3; i++) {
			Tranca tranca = new Tranca("000" + i, StatusTranca.DESTRANCADA);

			new TrancaRepository().insert(tranca);

			sentTrancaJsonSchemas.add(new TrancaJsonSchema(tranca));
		}

		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(null);

		TrancaController.getTranca(ctx);

		@SuppressWarnings("unchecked")
		ArgumentCaptor<List<TrancaJsonSchema>> argumentCaptor = ArgumentCaptor.forClass(List.class);

		verify(ctx).json(argumentCaptor.capture());
		verify(ctx).status(HttpStatus.OK_200);

		List<TrancaJsonSchema> capturedTrancaJsonSchemas = argumentCaptor.getValue();

		/*
		 * We need to sort both lists beforehands because List#equals
		 * only return true if both lists have the same elements AND they are in the
		 * same order
		 */
		Comparator<TrancaJsonSchema> trancaJsonSchemaComparator = new Comparator<TrancaJsonSchema>() {

			@Override
			public int compare(TrancaJsonSchema o1, TrancaJsonSchema o2) {
				return o1.getUuid().compareTo(o2.getUuid());
			}
		};

		Collections.sort(sentTrancaJsonSchemas, trancaJsonSchemaComparator);
		Collections.sort(capturedTrancaJsonSchemas, trancaJsonSchemaComparator);

		assertEquals(sentTrancaJsonSchemas, capturedTrancaJsonSchemas);
		verify(ctx).status(HttpStatus.OK_200);

	}

	@Test
	void testDeleteExistingTrancaReturnsJson() {
		Tranca tranca = new Tranca("0001", StatusTranca.DESTRANCADA);
		new TrancaRepository().insert(tranca);

		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema(tranca);
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(trancaJsonSchema);

		TrancaController.deleteTranca(ctx);

		ArgumentCaptor<TrancaJsonSchema> argumentCaptor = ArgumentCaptor
				.forClass(trancaJsonSchema.getClass());
		verify(ctx).json(argumentCaptor.capture());
		verify(ctx).status(HttpStatus.OK_200);

		TrancaJsonSchema capturedTrancaJsonSchema = argumentCaptor.getValue();
		TrancaJsonSchema expectedTrancaJsonSchema = new TrancaJsonSchema(tranca);

		assertEquals(expectedTrancaJsonSchema, capturedTrancaJsonSchema);
	}

	@Test
	void testDeleteNonExistentTrancaReturns404() {
		Tranca tranca = new Tranca("0001", StatusTranca.DESTRANCADA);

		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema(tranca);
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(trancaJsonSchema);

		TrancaController.deleteTranca(ctx);

		verify(ctx).status(HttpStatus.NOT_FOUND_404);
	}

	@Test
	void testDeleteInvalidTrancaReturns400() {
		when(ctx.bodyAsClass(Tranca.TrancaJsonSchema.class)).thenReturn(null);

		TrancaController.deleteTranca(ctx);

		verify(ctx).status(HttpStatus.BAD_REQUEST_400);
	}

}
