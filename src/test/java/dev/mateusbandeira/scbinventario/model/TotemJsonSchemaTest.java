package dev.mateusbandeira.scbinventario.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dev.mateusbandeira.scbinventario.model.Totem.TotemJsonSchema;

class TotemJsonSchemaTest {

	TotemJsonSchema totemJsonSchema;
	final String UUID_1 = "63879236-c5c5-4c9a-9749-0c46130b947d";
	final String NUMERO_1 = "0001";

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		totemJsonSchema = new TotemJsonSchema();

	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testTotemJsonSchemaTotem() {
		Totem totem = new Totem(UUID.fromString(UUID_1), NUMERO_1);
		totemJsonSchema = new TotemJsonSchema(totem);
		Totem asModel = totemJsonSchema.getAsModel();
		assertEquals(totem, asModel);
	}

	@Test
	void testGetAsModel() {
		totemJsonSchema.setNumero(NUMERO_1);
		totemJsonSchema.setUuid(UUID_1);

		Totem asModel = totemJsonSchema.getAsModel();
		Totem totem = new Totem(UUID.fromString(UUID_1), NUMERO_1);
		assertEquals(totem, asModel);
	}

}
