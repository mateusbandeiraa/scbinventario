package dev.mateusbandeira.scbinventario.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TotemTest {
	
	Totem totem;
	final UUID UUID_1 = UUID.fromString("63879236-c5c5-4c9a-9749-0c46130b947d");

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		totem = new Totem(UUID_1, "0001");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testWithoutUuid() {
		Totem withoutUuid = totem.withoutUuid();
		assertNotEquals(totem, withoutUuid);
		assertNull(withoutUuid.getUuid());
	}

}
