package dev.mateusbandeira.scbinventario.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dev.mateusbandeira.scbinventario.model.Tranca.TrancaJsonSchema;

class TrancaJsonSchemaTest {
	TrancaJsonSchema trancaJsonSchema;
	final String UUID_1 = "63879236-c5c5-4c9a-9749-0c46130b947d";
	final String NUMERO_1 = "0001";
	Tranca tranca;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		trancaJsonSchema = new TrancaJsonSchema();
		tranca = new Tranca(UUID.fromString(UUID_1), NUMERO_1, StatusTranca.DESTRANCADA);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testTrancaJsonSchemaTranca() {
		trancaJsonSchema = new TrancaJsonSchema(tranca);
		Tranca asModel = trancaJsonSchema.getAsModel();
		assertEquals(tranca, asModel);
	}

	@Test
	void testGetAsModelTrancada() {
		trancaJsonSchema.setUuid(UUID_1);
		trancaJsonSchema.setNumero(NUMERO_1);
		trancaJsonSchema.setOcupada(true);
		
		tranca.setStatus(StatusTranca.TRANCADA);
		
		assertEquals(tranca, trancaJsonSchema.getAsModel()); 
	}
	
	@Test
	void testGetAsModelDestrancada() {
		trancaJsonSchema.setUuid(UUID_1);
		trancaJsonSchema.setNumero(NUMERO_1);
		trancaJsonSchema.setOcupada(false);
		
		tranca.setStatus(StatusTranca.DESTRANCADA);
		
		assertEquals(tranca, trancaJsonSchema.getAsModel()); 
	}

	@Test
	void testIsOcupada() {
		tranca.setStatus(StatusTranca.TRANCADA);
		trancaJsonSchema = new TrancaJsonSchema(tranca);

		assertTrue(trancaJsonSchema.isOcupada());
		/*----*/
		tranca.setStatus(StatusTranca.DESTRANCADA);
		trancaJsonSchema = new TrancaJsonSchema(tranca);
		
		assertFalse(trancaJsonSchema.isOcupada());
	}
}
