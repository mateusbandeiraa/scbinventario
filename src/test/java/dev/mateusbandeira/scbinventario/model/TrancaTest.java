package dev.mateusbandeira.scbinventario.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TrancaTest {

	Tranca tranca;
	final UUID UUID_1 = UUID.fromString("63879236-c5c5-4c9a-9749-0c46130b947d");

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		tranca = new Tranca(UUID_1, "0001", StatusTranca.DESTRANCADA);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testWithoutUuid() {
		Tranca withoutUuid = tranca.withoutUuid();
		assertNull(withoutUuid.getUuid());
	}

}
