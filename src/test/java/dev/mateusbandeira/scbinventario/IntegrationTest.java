package dev.mateusbandeira.scbinventario;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import dev.mateusbandeira.scbinventario.model.StatusTranca;
import dev.mateusbandeira.scbinventario.model.Totem;
import dev.mateusbandeira.scbinventario.model.Totem.TotemJsonSchema;
import dev.mateusbandeira.scbinventario.model.Tranca;
import dev.mateusbandeira.scbinventario.model.Tranca.TrancaJsonSchema;
import dev.mateusbandeira.scbinventario.repository.TotemRepository;
import dev.mateusbandeira.scbinventario.repository.TrancaRepository;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

class IntegrationTest {
	private Application app = new Application();

	private static List<Totem> totems = new ArrayList<>();
	private static List<Tranca> trancas = new ArrayList<>();

	@BeforeAll
	static void beforeAll() {
		for (int i = 1; i <= 3; i++) {
			Totem totem = new Totem(null, "000" + i);
			totems.add(totem);
		}
		for (int i = 1; i <= 3; i++) {
			Tranca tranca = new Tranca("000" + i, StatusTranca.DESTRANCADA);
			trancas.add(tranca);
		}
	}
	
	@AfterEach
	void afterEach() {
		new TotemRepository().deleteAll();
		new TrancaRepository().deleteAll();
	}

	@Test
	void POST_tranca_returns_data() {
		app.start(1234);
		TrancaJsonSchema trancaJsonSchema = new TrancaJsonSchema();
		trancaJsonSchema.setNumero("0001");
		trancaJsonSchema.setOcupada(true);

		HttpResponse<JsonNode> httpResponse = Unirest.post(getBaseURL(1234) + "/tranca")
				.header("Content-Type", "application/json").body(trancaJsonSchema).asJson();
		assertEquals(httpResponse.getStatus(), HttpStatus.OK_200);

		Tranca trancaFromJson = JavalinJson
				.fromJson(httpResponse.getBody().toString(), Tranca.TrancaJsonSchema.class)
				.getAsModel();
		assertEquals(trancaFromJson.getNumero(), trancaJsonSchema.getNumero());
		assertEquals(trancaFromJson.getStatus(), trancaJsonSchema.getAsModel().getStatus());
		assertNotNull(trancaFromJson.getUuid());
		app.stop();
	}

	@Test
	void POST_totem_returns_data() throws InterruptedException {
		app.start(7000);
		TotemJsonSchema totemJsonSchema = new TotemJsonSchema();
		totemJsonSchema.setNumero("0001");

		HttpResponse<JsonNode> httpResponse = Unirest.post(getBaseURL(7000) + "/totem")
				.header("Content-Type", "application/json").body(totemJsonSchema).asJson();
		assertEquals(httpResponse.getStatus(), HttpStatus.OK_200);

		Totem totemFromJson = JavalinJson
				.fromJson(httpResponse.getBody().toString(), Totem.TotemJsonSchema.class)
				.getAsModel();

		assertEquals(totemFromJson.getNumero(), "0001");
		assertNotNull(totemFromJson.getUuid());
		app.stop();
	}
	
	@Test
	void GET_tranca_returns_data() {
		app.start(7002);
		for(Tranca tranca:trancas) {
			new TrancaRepository().insert(tranca);
		}
		HttpResponse<JsonNode> httpResponse = Unirest.get(getBaseURL(7002) + "/tranca")
				.header("Content-Type", "application/json").asJson();
		List<TrancaJsonSchema> trancaJsonSchemaFromJson = Arrays.asList(JavalinJson.fromJson(httpResponse.getBody().toString(), TrancaJsonSchema[].class));
		
		for(TrancaJsonSchema trancaJsonSchema : trancaJsonSchemaFromJson) {
			assertTrue(trancas.contains(trancaJsonSchema.getAsModel()));
		}
		
		for(Tranca tranca : trancas) {
			assertTrue(trancaJsonSchemaFromJson.contains(new Tranca.TrancaJsonSchema(tranca)));
		}
		
		assertTrue(trancas.size() == trancaJsonSchemaFromJson.size());
		app.stop();
	}
	@Test
	void GET_totem_returns_data() {
		app.start(7001);
		for(Totem totem:totems) {
			new TotemRepository().insert(totem);
		}
		HttpResponse<JsonNode> httpResponse = Unirest.get(getBaseURL(7001) + "/totem")
				.header("Content-Type", "application/json").asJson();
		List<TotemJsonSchema> totemJsonSchemasFromJson = Arrays.asList(JavalinJson.fromJson(httpResponse.getBody().toString(), TotemJsonSchema[].class));
		
		for(Totem.TotemJsonSchema totemJsonSchema : totemJsonSchemasFromJson) {
			assertTrue(totems.contains(totemJsonSchema.getAsModel()));
		}
		
		for(Totem totem : totems) {
			assertTrue(totemJsonSchemasFromJson.contains(new Totem.TotemJsonSchema(totem)));
		}
		
		assertTrue(totems.size() == totemJsonSchemasFromJson.size());
		app.stop();
	}
	@Test
	void applicationStarts() {
		Application.main(null);
	}
	
	private String getBaseURL(int port) {
		return "http://localhost:" + port;
	}

}
