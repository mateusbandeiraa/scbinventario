package dev.mateusbandeira.scbinventario.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dev.mateusbandeira.scbinventario.model.StatusTranca;
import dev.mateusbandeira.scbinventario.model.Tranca;

class TrancaRepositoryTest {
	private Tranca tranca1 = new Tranca(null, "0001", StatusTranca.DESTRANCADA);
	private Tranca tranca2 = new Tranca(null, "0002", StatusTranca.DESTRANCADA);
	private Tranca tranca3 = new Tranca(null, "0003", StatusTranca.DESTRANCADA);
	private TrancaRepository trancaRepository = new TrancaRepository();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		trancaRepository.deleteAll();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testInsertRetrieve() {
		trancaRepository.insert(tranca1);
		Tranca findById = trancaRepository.findById(tranca1.getUuid());
		assertEquals(tranca1, findById);
	}

	@Test
	void testInsertDuplicatedThrowsException() {
		trancaRepository.insert(tranca1);
		assertThrows(Repository.DuplicateEntryException.class,
				() -> trancaRepository.insert(tranca1));
	}

	@Test
	void testUpdate() {
		trancaRepository.insert(tranca1);
		Tranca modifiedTranca = new Tranca(tranca1.getUuid(), "MODIFIED", StatusTranca.DESTRANCADA);
		trancaRepository.insertOrUpdate(modifiedTranca.getUuid(), modifiedTranca);
		Tranca findById = trancaRepository.findById(tranca1.getUuid());
		assertEquals(modifiedTranca, findById);
	}

	@Test
	void testUpdateWithInvalidId() {
		trancaRepository.insert(tranca1);
		Tranca modifiedTranca = new Tranca(tranca1.getUuid(), "MODIFIED", StatusTranca.DESTRANCADA);
		assertThrows(IllegalArgumentException.class,
				() -> trancaRepository.insertOrUpdate(null, modifiedTranca));
	}

	@Test
	void testFindAll() {
		trancaRepository.insert(tranca1);
		trancaRepository.insert(tranca2);
		trancaRepository.insert(tranca3);

		List<Tranca> findAll = trancaRepository.findAll();
		assertEquals(3, findAll.size());
		assertTrue(findAll.contains(tranca1));
		assertTrue(findAll.contains(tranca2));
		assertTrue(findAll.contains(tranca3));
	}

	@Test
	void testDeleteById() {
		trancaRepository.insert(tranca1);
		trancaRepository.deleteById(tranca1.getUuid());
		Tranca findById = trancaRepository.findById(tranca1.getUuid());
		assertNull(findById);
	}

	@Test
	void testDeleteAll() {
		trancaRepository.insert(tranca1);
		trancaRepository.insert(tranca2);
		trancaRepository.insert(tranca3);

		trancaRepository.deleteAll();

		List<Tranca> findAll = trancaRepository.findAll();
		assertEquals(0, findAll.size());
	}
}
