package dev.mateusbandeira.scbinventario.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dev.mateusbandeira.scbinventario.model.Totem;

class TotemRepositoryTest {

	private Totem totem1 = new Totem(null, "0001");
	private Totem totem2 = new Totem(null, "0002");
	private Totem totem3 = new Totem(null, "0003");
	private TotemRepository totemRepository = new TotemRepository();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		totemRepository.deleteAll();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testInsertRetrieve() {
		totemRepository.insert(totem1);
		Totem findById = totemRepository.findById(totem1.getUuid());
		assertEquals(totem1, findById);
	}

	@Test
	void testInsertDuplicatedThrowsException() {
		totemRepository.insert(totem1);

		assertThrows(Repository.DuplicateEntryException.class,
				() -> totemRepository.insert(totem1));
	}

	@Test
	void testUpdate() {
		totemRepository.insert(totem1);
		Totem modifiedTotem = new Totem(totem1.getUuid(), "MODIFIED");
		totemRepository.insertOrUpdate(modifiedTotem.getUuid(), modifiedTotem);
		Totem findById = totemRepository.findById(totem1.getUuid());
		assertEquals(modifiedTotem, findById);
	}

	@Test
	void testUpdateWithInvalidId() {
		totemRepository.insert(totem1);
		Totem modifiedTotem = new Totem(totem1.getUuid(), "MODIFIED");
		assertThrows(IllegalArgumentException.class,
				() -> totemRepository.insertOrUpdate(null, modifiedTotem));
	}

	@Test
	void testFindAll() {
		totemRepository.insert(totem1);
		totemRepository.insert(totem2);
		totemRepository.insert(totem3);

		List<Totem> findAll = totemRepository.findAll();
		assertEquals(3, findAll.size());
		assertTrue(findAll.contains(totem1));
		assertTrue(findAll.contains(totem2));
		assertTrue(findAll.contains(totem3));
	}

	@Test
	void testDeleteById() {
		totemRepository.insert(totem1);
		totemRepository.deleteById(totem1.getUuid());
		Totem findById = totemRepository.findById(totem1.getUuid());
		assertNull(findById);
	}

	@Test
	void testDeleteAll() {
		totemRepository.insert(totem1);
		totemRepository.insert(totem2);
		totemRepository.insert(totem3);

		totemRepository.deleteAll();

		List<Totem> findAll = totemRepository.findAll();
		assertEquals(0, findAll.size());
	}

}
